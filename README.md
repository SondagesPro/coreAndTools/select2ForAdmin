# select2ForAdmin

Activate select2 on some dropdown for admin user. To activate dropdown : activate plugin. This can be interseting if you have a lot of surveys, users …

Currently, select2 is activated with minimum results for search to 8 on

- Copy survey list
- User select
- User group select
- Theme selector

# Home page & Copyright
- HomePage <https://sondages.pro/>
- Support and pull request <https://gitlab.com/SondagesPro/coreAndTools/select2ForAdmin>
- Copyright © 2020 Denis Chenu <http://sondages.pro>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/) 
