/* select2 on cool select */
$(document).on('ready pjax:scriptcomplete',function()
{
    /* Copy survey */
    $("select#copysurveylist").select2({ theme:'bootstrap', minimumResultsForSearch : 8 });
    /* Permission */
    $("select#uidselect").select2({ theme:'bootstrap', minimumResultsForSearch : 8 });
    $("select#ugidselect").select2({ theme:'bootstrap', minimumResultsForSearch : 8 });
    /* Template selector */
    $("select#template").select2({ theme:'bootstrap', minimumResultsForSearch : 8 });
    $("select#templatedir").select2({ theme:'bootstrap', minimumResultsForSearch : 8 });
    /* Theme options */
    $("select.selector_image_selector").select2({ theme:'bootstrap', minimumResultsForSearch : 8 });
    /* Group filter */
    $("#Survey_gsid").select2({ theme:'bootstrap', minimumResultsForSearch : 8 });
    /* Know plugin */
    $("select#selectsurvey").select2({ theme:'bootstrap',minimumResultsForSearch : 8 });
});
