<?php
/**
 * select2ForAdmin : Activate select2 on some dropdown for admin user.
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2023 Denis Chenu <http://www.sondages.pro>

 * @license GPL v3
 * @version 0.2.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class select2ForAdmin extends PluginBase {


    static protected $description = 'Activate select2 for admin user.';
    static protected $name = 'select2ForAdmin';

    /** @inheritdoc */
    public function init()
    {
        $this->subscribe('beforeAdminMenuRender');
    }
    /*
     * Register script
     */
    public function beforeAdminMenuRender()
    {
        Yii::setPathOfAlias('select2admin',dirname(__FILE__));
        if(!App()->clientScript->hasPackage('select2admin')) {
            App()->clientScript->addPackage( 'select2admin', array(
                'basePath'    => 'select2admin.assets',
                'css'         => array('select2ForAdmin.css'),
                'js'          => array('select2ForAdmin.js'),
                'depends'     => array(
                    'jquery',
                    'adminbasics'
                )
            ));
        }
        App()->clientScript->registerPackage('select2admin');
    }

}
